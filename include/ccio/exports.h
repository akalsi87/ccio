/*! exports.h */

#ifndef _CCIO_EXPORTS_H_
#define _CCIO_EXPORTS_H_

#if defined(USE_CCIO_STATIC)
#  define CCIO_API
#elif defined(_WIN32) && !defined(__GCC__)
#  ifdef BUILDING_CCIO_SHARED
#    define CCIO_API __declspec(dllexport)
#  else
#    define CCIO_API __declspec(dllimport)
#  endif
#  ifndef _CRT_SECURE_NO_WARNINGS
#    define _CRT_SECURE_NO_WARNINGS
#  endif
#else
#  ifdef BUILDING_CCIO_SHARED
#    define CCIO_API __attribute__((visibility ("default")))
#  else
#    define CCIO_API
#  endif
#endif

#if defined(__cplusplus)
#  define CCIO_EXTERN_C extern "C"
#  define CCIO_C_API CCIO_EXTERN_C CCIO_API
#else
#  define CCIO_EXTERN_C
#  define CCIO_C_API CCIO_API
#endif

CCIO_EXTERN_C CCIO_C_API const char CCIO_VERSION[];

#endif/*_CCIO_EXPORTS_H_*/
